package tp4;

public class Pokemon {
	//attribut
	private int energie;
	private int maxEnergie;
	boolean vivre;
	String nom;
	public int cycle;
	private int puissance;

	//accesseurs
	String getNom() {
		return nom;
	}
	int getEnergie() {
		return energie;
	}
	 int getPuissance() {
		 return puissance;
	 }

	//constructeur
	public Pokemon(String nom) {
		this.nom = nom;
		maxEnergie = 50 + (int)(Math.random() * ((90 - 50) + 1));
		energie = 30 + (int)(Math.random() * ((maxEnergie - 30) + 1));
		puissance=3 + (int)(Math.random() * ((10 - 3) + 1)); 
	}
	//methode
	void sePresenter() {
		System.out.println("Je suis "+getNom()+" j'ai "+energie+" points d'energie ("+maxEnergie+" max) et une puissance de "+puissance);
	}
	void manger() {
		//nombreAleatoire = Min + (int)(Math.random() * ((Max - Min) + 1)); 
		int nbBaies = 0;
		nbBaies =10 + (int)(Math.random() * ((30 - 10) + 1));
		energie=energie+nbBaies;
		if (energie>maxEnergie) {
			energie=maxEnergie;
		}
	}
	void vivre() {
		
		energie = energie - (20 + (int)(Math.random() * ((40-20) + 1))); 
		if(energie<0) {
			energie=0;
		}
	}
	boolean isAlive() {
		if(energie>0) {
			return vivre=true;
		}
		else {
			return vivre=false;
		}
	}
	void nombre_cycle_vecus() {
		cycle=0;
		while(energie >0) {
			manger();
			vivre();
			cycle+=1;
		}
		System.out.println(nom+" a v�cu "+cycle+" cycles !");
	}
	void perdreEnergie(int perte) {
		energie=energie-perte;
	}
	void attaquer(Pokemon adversaire) {
		adversaire.perdreEnergie(puissance);
	}


}


