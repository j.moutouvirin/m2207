package tp2;

public class Cercle extends Form {
	
	//attribut
	private double rayon;
	
	//constructeur
	Cercle(){
		super();
		rayon=1.0;
	}
	//accesseur
	public double getRayon() {
		return rayon;
	}
	void setRayon(double r) {
		rayon=r;
	}
}
