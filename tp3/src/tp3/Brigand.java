package tp3;

public class Brigand extends Humain {
	//attributs

	//Jusqu'� l'exercice 7.2, on utilise private au lieu de protected
	protected String look;
	private int dame_kidnappees;
	private int recompense;
	private boolean prison;

	//constructeur
	public Brigand(String nom) {
		super(nom);
		look="m�chant";
		prison=false;
		recompense=100;
		dame_kidnappees=0;
		boisson_favorite="Cognac";
	}

	//methodes
	int getRecompense() {
		return recompense;
	}
	String quelEstTonNom() {
		return nom+" le "+look;
	}
	void sePresenter() {
		super.sePresenter();
		parler("J'ai l'air "+look+" et j'ai enlev� "+dame_kidnappees+" dames.");
		parler("Ma t�te est mise � prix "+getRecompense()+"$ !!");
	}
	void enleve(Dame dame) {
		dame.libre=false;
		dame_kidnappees+=1;
		recompense+=100;
		parler("Ah ah ! "+dame.nom+", tu es ma prisonni�re !" );
	}

	//exercice 8.2
	void emprisonner(Sherif sherif) {
		System.out.println("Damned, je suis fait ! "+sherif.quelEstTonNom()+", tu m�as eu !");
		prison=true;
	}

}
