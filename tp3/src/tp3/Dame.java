package tp3;

public class Dame extends Humain{


	//Attribut
	protected boolean libre; //jusqu'au 6.2 on met private � la place de protected

	//constructeur
	public Dame(String nom) {
		super(nom);
		boisson_favorite="Martini";
		libre=true;
	}
	void priseEnOtage() {
		libre=false;
		parler("Au secours ! ");
	}
	void estLiberee() {
		libre=true;
		parler(" Merci Cowboy ");
	}
	
	String etatCapture() {
		if (libre=true) {
			return "libre";
		}
		else {
			return"kidnapp�e";
		}
	}
	
	//methode
	String quelEstTonNom(){
		return "Miss "+nom;
	}
	void sePresenter() {
		super.sePresenter();
		parler("Actuellement, je suis "+etatCapture());
	}
}
