package tp3;

public class Cowboy extends Humain{
	//attribut
	protected int popularite; //remplacement deprivate par protected � partir de l'exercice 8.4
	protected String caracteristique;//cr�er � partir de l'exercice 7.3

	//constructeur
	public Cowboy(String nom) {
		super (nom);
		boisson_favorite="Whiskey";
		popularite=0;
		caracteristique="vaillant";//cr�er � partir de l'exercice 7.3
	}

	//methode
	//exercice 7.3
	void tire(Brigand brigand) {
		System.out.println("Le "+caracteristique+" "+nom+" tire sur "+brigand.nom+". PAN !");
		parler("Prend �a, voyou !");
	}
	//exercice 7.6
	void libere(Dame dame) {
		dame.libre=true;
		popularite+=10;
	}
}
