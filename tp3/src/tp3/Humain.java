package tp3;

public class Humain {
	//attributs
	protected String nom;
	protected String boisson_favorite;
	
	
	//constructeurs
	public Humain(String nom) {
		boisson_favorite="lait";
		this.nom=nom;
	}
	
	//methode
	 String quelEstTonNom() {
		 return nom;
	 }
	 String quelleEstTaBoisson() {
		 return boisson_favorite;
	 }
	 void parler(String texte) {
		 System.out.println(nom+" - "+texte);
	 }
	 void sePresenter() {
		 parler("Bonjour, je suis "+quelEstTonNom()+" et ma boisson pr�f�r�e est le "+quelleEstTaBoisson());
	 }
	 void boire() {
		 parler("Ah ! un bon verre de "+quelleEstTaBoisson()+" ! GLOUPS !");
	 }
	 
}
