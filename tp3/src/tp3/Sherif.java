package tp3;

public class Sherif extends Cowboy{
	//attributs
	protected int brigands_arretes;
	protected String boisson;
	//construteur
	public Sherif(String nom) {
		super(nom);
		brigands_arretes=0;

	}

	//methodes
	String quelEstTonNom() {
		return "Sherif "+nom;
	}

	void sePresentee() {
		super.sePresenter();;
		parler("Je suis "+caracteristique+" et ma popularit� est "+popularite);
		parler("J'ai d�j� arr�t� "+brigands_arretes+" brigand");
	}
	//exercice 8.3
	void coffrer(Brigand b) {
		System.out.println("Au nom de la loi, je vous arr�te, "+b.quelEstTonNom());
		brigands_arretes+=1;
	}

}
