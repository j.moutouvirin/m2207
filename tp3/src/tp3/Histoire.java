package tp3;

public class Histoire {

	public static void main(String[] args) {
		Humain bob=new Humain("Bob");
		Dame rachel=new Dame("Rachel");
		Brigand gunther=new Brigand("Gunther");
		Cowboy simon=new Cowboy("Simon");
		bob.sePresenter();
		bob.boire();
		Sherif marshall=new Sherif("Marshall");
		//exercice  9
		Cowboy clint = new Sherif("Clint");
		
		
		//exercice 2.4
		rachel.sePresenter();
		rachel.priseEnOtage();
		rachel.estLiberee();
		
		//exercice 3.2
		gunther.sePresenter();
		
		
		//exercice 4.3
		simon.sePresenter();
		
		//exercice 6.2
		rachel.sePresenter();
		
		//exercice 7.2
		gunther.enleve(rachel);
		gunther.sePresenter();
		
		//exercice 7.5
		simon.tire(gunther);
		simon.libere(rachel);
		rachel.estLiberee();
		rachel.sePresenter();
		
		//exercice 8.1 et 8.4
		marshall.sePresentee();
		marshall.coffrer(gunther);
		gunther.emprisonner(marshall);
		marshall.sePresentee();
		
		//exercice 9
		// Les m�thodes possibles sont boire(), libere(Dame dame),parler(String texte),
		//quelEstTonNom(), quelleEstTaBoisson(), sePresenter(),tire(Brigand brigand).
		//On ne peut pas lui demander des m�thodes propres au Sherif comme coffrer(Brigand b) car
		//clint est un cowboy.
		
	}

}
